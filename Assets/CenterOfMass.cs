﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOfMass : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rigidbody = GetComponentInParent<Rigidbody>();
        if (rigidbody != null)
            rigidbody.centerOfMass = this.transform.localPosition;
    }


}
