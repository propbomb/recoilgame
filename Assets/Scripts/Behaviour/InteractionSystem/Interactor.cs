﻿public interface Interactor
{

    void onInteractionCompleted(Interaction interaction, Interaction.Result status);
    void onInteractionStarted(Interaction interaction, Interaction.Result status);
    void onInteractionHeld(Interaction interaction, Interaction.Result status);

}
