﻿using UnityEngine;

public abstract class Interaction : MonoBehaviour
{
    public string label { get; set; }
    public string description { get; set; }
    public string hint { get; set; }
    public bool allowLifecycleAutoFix = false;
    private static int LAST_USEAGE_DEFAULT = -1;
    private int lastUseage = LAST_USEAGE_DEFAULT;
    private int useageTolerance = 1;
    private bool hasStarted = false;

    public enum Result
    {
        SUCCESSFUL,
        FAILED,
        UNKNOWN,
        PARTIAL
    }

    
    public Interaction(string label, string description, string hint, bool shouldAllowLifecycleAutoFix = false)
    {
        this.label = label;
        this.description = description;
        this.hint = hint;
    
        this.allowLifecycleAutoFix = shouldAllowLifecycleAutoFix;
    }

    protected abstract Result onInteractionStart(Interactor source);
    protected abstract Result onInteractionRepeat(Interactor source);
    protected abstract Result onInteractionComplete(Interactor source);
    public abstract void onDiscovered(Interactor source);

    protected Result startInteraction(Interactor source)
    {
        Result result = onInteractionStart(source);
        source.onInteractionStarted(this, result);
        return result;
    }

    protected Result holdInteraction(Interactor source)
    {
        Result result = onInteractionRepeat(source);
        source.onInteractionHeld(this, result);
        return result;
    }

    public Result completeInteraction(Interactor source)
    {
        Result result = onInteractionComplete(source);
        source.onInteractionCompleted(this, result);
        return result;
    }

    public bool isContinuous()
    {
        return (lastUseage + useageTolerance) >= Time.frameCount;
    }
    
    protected abstract void handleInteraction(Interactor source);


    public void interact(Interactor interactor)
    {
        handleInteraction(interactor);
        lastUseage = Time.frameCount;
    }


}
