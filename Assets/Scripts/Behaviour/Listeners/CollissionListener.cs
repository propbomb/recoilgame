﻿using UnityEngine;

public abstract class CollissionListener
{
    public virtual void OnCollisionEnter(Collision collision)
    {

    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {

    }

    public virtual void OnCollisionExit(Collision collision)
    {

    }

    public virtual void OnCollisionExit2D(Collision2D collision)
    {

    }

    public virtual void OnCollisionStay(Collision collision)
    {

    }

    public virtual void OnCollisionStay2D(Collision2D collision)
    {

    }

    public virtual void OnControllerColliderHit(ControllerColliderHit hit)
    {

    }

    public virtual void OnTriggerEnter(Collider other)
    {

    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {

    }

    public virtual void OnTriggerExit(Collider other)
    {

    }

    public virtual void OnTriggerExit2D(Collider2D collision)
    {

    }

    public virtual void OnTriggerStay(Collider other)
    {

    }

    public virtual void OnTriggerStay2D(Collider2D collision)
    {

    }
}
