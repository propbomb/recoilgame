﻿using UnityEngine;

public class InputService
{
    public class Axes
    {
        public static string horizontalAxis = "Horizontal";
        public static string verticalAxis = "Vertical";
        public static string fire1 = "Fire1";
        public static string fire2 = "Fire2";
        public static string fire3 = "Fire3";
        public static string jump = "Jump";
        public static string mouseX = "Mouse X";
        public static string mouseY = "Mouse Y";
        public static string mouseScrollWheel = "Mouse ScrollWheel";
        public static string submit = "Submit";
        public static string cancel = "Cancel";
        public static string use = "Use";
        public static string propRotate = "PropRotate";
    }
    public float getAxis(string axis)
    {
        return Input.GetAxis(axis);
    }

    public bool getButton(string button)
    {
        return Input.GetButton(button);
    }

    public float getMouseXDelta()
    {
        return Input.GetAxis("Mouse X");
    }

    public float getMouseYDelta()
    {
        return Input.GetAxis("Mouse Y");
    }
}
