﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public float explosionRadius = 30.0f;
    public float explosionTime = 10.0f;
    public float elapsedTime = 0.0f;
    public float explosionForce = 1000.0f;
    public bool active = false;
}
