﻿using Components;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Vector3 localPickupPosition;
    public float pickupPower = 100.0f;
    public float maxHoldingDistance = 10.0f;
    public string pickupAxis = InputService.Axes.use;
    public string throwAxis = InputService.Axes.fire1;
    public float throwVelocity = 500.0f;
    public PhysicsProp pickedUpProp = null;
    public Vector3 getWorldPickupPosition()
    {
        return this.transform.position + this.transform.rotation * localPickupPosition;
    }
}
