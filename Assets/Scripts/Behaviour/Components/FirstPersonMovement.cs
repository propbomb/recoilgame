﻿using UnityEngine;
namespace Components
{
    public class FirstPersonMovement : CollissionSubject
    {
        public float forwardSpeed = 10.0f;
        public float backwardSpeed = 10f;
        public float sidewaysSpeed = 10f;
        public float acceleration = 40.0f;
        public float deacceleration = 80.0f;
        public GameObject directionReference;
        public float rigidbodyPushPower = 1.0f;
        public float jumpPower = 100.0f;
        public float jumpControlDuration = 0.1f;
        public float jumpControlTimer = 0.0f;
        public bool isCollidingSides = false;


        public float airControl = 10.0f;

        public Vector3 currentVelocity = Vector3.zero;
    }
}