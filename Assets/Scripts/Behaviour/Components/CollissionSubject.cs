﻿using System.Collections.Generic;
using UnityEngine;

public class CollissionSubject : MonoBehaviour
{
    private List<CollissionListener> listeners = new List<CollissionListener>();

    public void listen(CollissionListener listener)
    {
        if (!this.listeners.Contains(listener))
            this.listeners.Add(listener);
    }

    public void stopListening(CollissionListener listener)
    {
        this.listeners.Remove(listener);
    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionEnter(collision);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionEnter2D(collision);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionExit(collision);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionExit2D(collision);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionStay(collision);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnCollisionStay2D(collision);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnControllerColliderHit(hit);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerEnter(other);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerEnter2D(collision);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerExit(other);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerExit2D(collision);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerStay(other);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        foreach (CollissionListener listener in listeners)
        {
            listener.OnTriggerStay2D(collision);
        }
    }
}
