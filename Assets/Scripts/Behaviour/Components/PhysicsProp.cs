﻿using UnityEngine;
namespace Components
{
    public class PhysicsProp : Interaction
    {
        public bool pickupAllowed = true;
        public bool pickedUp = false;
        public string propName = "NO_NAME";
        public float pickupDifficulty = 100.0f;
        public Pickup pickup;
        private InputService inputService;
        public bool pickupKeyHelper = true;
        public bool justDropped = false;
        public bool colliding = false;
        public Collision collisionObject;
        private int lastInteraction = 0;
        public Vector3 rotationalForce;


        public void OnCollisionEnter(Collision collision)
        {
            colliding = true;
            collisionObject = collision;
        }

        private void OnCollisionExit(Collision collision)
        {
            colliding = false;
        }
        public PhysicsProp()
            : base("PropPickup-", "Pick up ", "Press e to pick up")
        {
            this.label = "PropPickup-" + propName;
            this.description = "Pick up " + propName;
            this.inputService = new InputService();
            this.pickupKeyHelper = true;
        }

        public override void onDiscovered(Interactor source)
        {
            Debug.Log("Prop discovered");
        }

        protected override Result onInteractionComplete(Interactor source)
        {
            justDropped = true;
            pickedUp = false;
            pickupKeyHelper = true;
            colliding = false;
            collisionObject = null;

            return Result.SUCCESSFUL;
        }

        protected override Result onInteractionRepeat(Interactor source)
        {
            return Result.SUCCESSFUL;
        }

        protected override Result onInteractionStart(Interactor source)
        {
            
            if (!justDropped)
            {
                pickedUp = true;
                
                return Result.SUCCESSFUL;
            }
            return Result.FAILED;
        }
        protected override void handleInteraction(Interactor source)
        {
            MonoBehaviour monoBehaviour = source as MonoBehaviour;
            this.pickup = monoBehaviour.GetComponentInParent<Pickup>();
            Rigidbody rigidbody = this.GetComponent<Rigidbody>();
            
            if (pickup != null && rigidbody != null && pickupAllowed)
            {
                if(pickedUp == false)
                {
                    startInteraction(source);
                }
                else
                {
                    holdInteraction(source);
                }
                lastInteraction = Time.frameCount;
            }
        }
    }
}