﻿using UnityEngine;
namespace Components
{
    public class RaycastInteractor : MonoBehaviour, Interactor
    {
        public GameObject raycastSource;
        public float raycastLength = 10.0f;
        public float raycastThickness = 0.0f;
        public Vector3 raycastAngle = new Vector3(0f, 0f, 0f);

        public float discoveryRadius = 10.0f;
        public string useKey = InputService.Axes.use;

        public void onInteractionCompleted(Interaction interaction, Interaction.Result status)
        {
        }

        public void onInteractionHeld(Interaction interaction, Interaction.Result status)
        {
        }

        public void onInteractionStarted(Interaction interaction, Interaction.Result status)
        {
        }
    }
}