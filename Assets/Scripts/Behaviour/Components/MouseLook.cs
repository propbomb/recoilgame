﻿using UnityEngine;
namespace Components
{
    public class MouseLook : MonoBehaviour
    {
        public float mouseSensitvity = 1.0f;
        public string yawAxis = InputService.Axes.mouseX;
        public string pitchAxis = InputService.Axes.mouseY;


        public float yawClamp = 360;
        public float pitchClamp = 90;

        public Vector2 currentRotation = Vector2.zero;


    }
}