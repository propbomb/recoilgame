﻿using Unity.Entities;
using UnityEngine;

namespace Systems
{
    public class MouseLookSystem : ComponentSystem
    {
        private InputService input;


        public MouseLookSystem()
        {
            this.input = new InputService();
        }


        protected override void OnUpdate()
        {
            Entities.ForEach<Transform, Components.MouseLook>((Entity e, Transform t, Components.MouseLook mouseLookComponent) =>
            {
                if (!input.getButton(InputService.Axes.propRotate)) { 

                    if (Cursor.lockState == CursorLockMode.None)
                    Cursor.lockState = CursorLockMode.Locked;
                    if (mouseLookComponent.currentRotation == Vector2.zero)
                    {
                        mouseLookComponent.currentRotation = t.eulerAngles;
                    }

                    float pitchDelta = -input.getAxis(mouseLookComponent.pitchAxis);
                    float yawDelta = input.getAxis(mouseLookComponent.yawAxis);

                    pitchDelta *= mouseLookComponent.mouseSensitvity;
                    yawDelta *= mouseLookComponent.mouseSensitvity;


                    mouseLookComponent.currentRotation.x += pitchDelta;
                    mouseLookComponent.currentRotation.y += yawDelta;

                    if (Mathf.Abs(mouseLookComponent.currentRotation.x) >= 360)
                        mouseLookComponent.currentRotation.x = Mathf.Abs(mouseLookComponent.currentRotation.x) - 360;

                    if (Mathf.Abs(mouseLookComponent.currentRotation.y) >= 360)
                        mouseLookComponent.currentRotation.y = Mathf.Abs(mouseLookComponent.currentRotation.y) - 360;

                    float pitchClamped = Mathf.Clamp(mouseLookComponent.currentRotation.x, -mouseLookComponent.pitchClamp, mouseLookComponent.pitchClamp);
                    float yawClamped = Mathf.Clamp(mouseLookComponent.currentRotation.y, -mouseLookComponent.yawClamp, mouseLookComponent.yawClamp);


                    mouseLookComponent.currentRotation.x = pitchClamped;
                    mouseLookComponent.currentRotation.y = yawClamped;

                    t.eulerAngles = mouseLookComponent.currentRotation;
                }

            });
        }

    }
}