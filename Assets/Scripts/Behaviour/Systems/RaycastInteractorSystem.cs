﻿using Unity.Entities;
using UnityEngine;
namespace Systems
{
    public class RaycastInteractorSystem : ComponentSystem
    {
        private InputService input;
        private Interaction currentInteraction = null;
        public RaycastInteractorSystem()
        {
            this.input = new InputService();
        }


        protected override void OnUpdate()
        {
            Entities.ForEach((Entity ent, Components.RaycastInteractor raycastInteractorComponent) =>
            {
                GameObject source = raycastInteractorComponent.raycastSource;
                RaycastHit hit;
                int layerMask = 1 << 9;
                layerMask = ~layerMask;
                bool isInteracting = input.getButton(raycastInteractorComponent.useKey);
                Quaternion rotx = Quaternion.AngleAxis(raycastInteractorComponent.raycastAngle.x, Vector3.right);
                Quaternion roty = Quaternion.AngleAxis(raycastInteractorComponent.raycastAngle.y, Vector3.up);
                Quaternion rotz = Quaternion.AngleAxis(raycastInteractorComponent.raycastAngle.z, Vector3.forward);
                Quaternion rot = rotx * roty * rotz;

                Vector3 raycastDir = rot * source.transform.forward;

                Debug.DrawLine(source.transform.position, source.transform.position + raycastDir * raycastInteractorComponent.raycastLength, Color.magenta);

                if (Physics.SphereCast(source.transform.position, raycastInteractorComponent.raycastThickness, raycastDir, out hit, raycastInteractorComponent.raycastLength, layerMask))
                {
                    Debug.DrawLine(hit.point, hit.point + hit.normal * 20, Color.green);


                    MonoBehaviour[] scripts = hit.collider.gameObject.GetComponents<MonoBehaviour>();
                    foreach (MonoBehaviour script in scripts)
                    {
                        if (script is Interaction)
                        {
                            Interaction interactable = (Interaction)script;

                            if (isInteracting)
                            {
                                interactable.interact(raycastInteractorComponent);
                            }
                        }
                    }


                }


            });
        }
    }
}