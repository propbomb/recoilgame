﻿using System;
using Unity.Entities;
using UnityEngine;

class FirstPersonMovementServiceCollissionListener : CollissionListener, IEquatable<FirstPersonMovementServiceCollissionListener>
{
    private Components.FirstPersonMovement firstPersonMovementComponent;
    private CharacterController characterController;

    public FirstPersonMovementServiceCollissionListener(Components.FirstPersonMovement firstPersonMovementComponent, CharacterController characterController)
    {
        this.firstPersonMovementComponent = firstPersonMovementComponent;
        this.characterController = characterController;
    }

    public bool Equals(FirstPersonMovementServiceCollissionListener other)
    {
        return other.characterController == this.characterController;
    }
    public override bool Equals(object obj)
    {
        return Equals(obj as FirstPersonMovementServiceCollissionListener);
    }

    public override int GetHashCode()
    {
        return this.characterController.GetHashCode();
    }
    public override void OnCollisionEnter(Collision collision)
    {
        return;
    }


    public override void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.rigidbody != null)
            hit.rigidbody.AddForceAtPosition(this.firstPersonMovementComponent.rigidbodyPushPower * firstPersonMovementComponent.currentVelocity, hit.point, ForceMode.Force);

        if (firstPersonMovementComponent.isCollidingSides)
        {
            /*Vector3 characterRefPoint = characterController.transform.position;
            Vector3 hitToCharacter = characterRefPoint - hit.point;
         

            Debug.Log(characterController.transform.position + " - " + hit.point + " = " + hitToCharacter);


            Debug.DrawLine(hit.point, hit.point + hit.normal, Color.magenta, 10.0f);
            Debug.DrawLine(characterController.transform.position, characterController.transform.position + new Vector3(0f, 1f, 0f), Color.yellow);
            Debug.DrawLine(characterController.transform.position, characterController.transform.position + hitToCharacter, Color.magenta);
            firstPersonMovementComponent.currentVelocity += hitToCharacter.normalized * Vector3.Scale(firstPersonMovementComponent.currentVelocity, hitToCharacter.normalized).magnitude;
            Debug.DrawLine(characterController.transform.position, characterController.transform.position + hitToCharacter.normalized * Vector3.Scale(firstPersonMovementComponent.currentVelocity, hitToCharacter.normalized).magnitude, Color.red, 10.0f);
            *///firstPersonMovementComponent.currentVelocity = Vector3.Cross(hit.normal, Vector3.up).normalized * firstPersonMovementComponent.currentVelocity.magnitude;
        }
    }
}
namespace Systems
{
    public class FirstPersonMovement : ComponentSystem
    {
        private InputService input;

        public FirstPersonMovement()
        {
            this.input = new InputService();
        }
        public static void drawVector(Vector3 vector, Vector3 origin, Vector3 offset, Color color)
        {
            Debug.DrawLine(origin + offset, origin + vector + offset, color);
        }
        private Vector3 applySpeedVariables(Vector3 input, Vector2 playerInput, Components.FirstPersonMovement firstPersonMovementComponent, CharacterController character)
        {
            if (!character.isGrounded)
                return input * firstPersonMovementComponent.airControl;
            if (playerInput.x > 0.1)
            {
                return input * firstPersonMovementComponent.forwardSpeed;
            }
            if (playerInput.x < -0.1)
            {
                return input * firstPersonMovementComponent.backwardSpeed;
            }

            if (Mathf.Abs(playerInput.y) > 0.1)
                return input * firstPersonMovementComponent.sidewaysSpeed;
            return input;
        }

        protected override void OnUpdate()
        {
            Entities.ForEach((Entity e, CharacterController character, Components.FirstPersonMovement firstPersonMovementComponent) =>
            {
                
                firstPersonMovementComponent.listen(new FirstPersonMovementServiceCollissionListener(firstPersonMovementComponent, character));
                Transform t = character.transform;
                if (firstPersonMovementComponent.directionReference != null)
                {
                    t = firstPersonMovementComponent.directionReference.transform;
                }

                Vector2 input = new Vector2(this.input.getAxis(InputService.Axes.verticalAxis),
                                                this.input.getAxis(InputService.Axes.horizontalAxis));


                Vector3 forwardGrounded = (new Vector3(t.forward.x, 0.0f, t.forward.z)).normalized;


                Vector3 forwardMovement = forwardGrounded * input.x;

                Vector3 leftRightMovement = t.transform.right * input.y;

                Vector3 targetVelocity = forwardMovement + leftRightMovement;
                if (targetVelocity.magnitude > 1.0)
                    targetVelocity = targetVelocity.normalized;
                targetVelocity = applySpeedVariables(targetVelocity, input, firstPersonMovementComponent, character);

                Debug.DrawLine(t.position, t.position + targetVelocity, Color.red);


                Vector3 currentVelocity = new Vector3(firstPersonMovementComponent.currentVelocity.x, 0.0f, firstPersonMovementComponent.currentVelocity.z);

                Vector3 interp = Vector3.MoveTowards(currentVelocity, targetVelocity, (targetVelocity.magnitude > 0.1 ? firstPersonMovementComponent.acceleration : firstPersonMovementComponent.deacceleration) * Time.deltaTime);
                if (!character.isGrounded)
                {
                    if (targetVelocity.magnitude > 0.1f)
                    {
                        float oldMagnitude = currentVelocity.magnitude;
                        interp = currentVelocity + targetVelocity * Time.deltaTime * firstPersonMovementComponent.airControl;
                        if (interp.magnitude > oldMagnitude && interp.magnitude > firstPersonMovementComponent.airControl)
                            interp = interp.normalized * oldMagnitude;
                    }
                    else
                        interp = currentVelocity;
                }
                Vector3 gravity = character.transform.up * (firstPersonMovementComponent.currentVelocity.y - 20.0f * Time.deltaTime);


                firstPersonMovementComponent.currentVelocity = interp + gravity;


                if (character.isGrounded && this.input.getButton(InputService.Axes.jump) || this.input.getButton(InputService.Axes.jump) && firstPersonMovementComponent.jumpControlTimer < firstPersonMovementComponent.jumpControlDuration && firstPersonMovementComponent.jumpControlTimer > 0)
                {
                    firstPersonMovementComponent.currentVelocity += firstPersonMovementComponent.jumpPower * character.transform.up * Time.deltaTime;

                    firstPersonMovementComponent.jumpControlTimer += Time.deltaTime;
                }
                else
                    firstPersonMovementComponent.jumpControlTimer = 0.0f;

                //            firstPersonMovementComponent.currentVelocity.y = Mathf.Clamp(firstPersonMovementComponent.currentVelocity.y, -55.0f, 55.0f);

                CollisionFlags flag = character.Move(firstPersonMovementComponent.currentVelocity * Time.deltaTime);

                if ((flag & CollisionFlags.Above) != 0 || (flag & CollisionFlags.Below) != 0)
                {
                    firstPersonMovementComponent.currentVelocity.y = 0;
                }

                if ((flag & CollisionFlags.Sides) != 0)
                {
                    firstPersonMovementComponent.isCollidingSides = true;
                }
                else
                {
                    firstPersonMovementComponent.isCollidingSides = false;
                }


                drawVector(gravity, character.transform.position, new Vector3(0.3f, 0.0f, 0.0f), Color.yellow);
                drawVector(currentVelocity, character.transform.position, new Vector3(0.4f, 0.0f, 0.0f), Color.gray);
                drawVector(interp, character.transform.position, new Vector3(0.5f, 0.0f, 0.0f), Color.green);

                //Debug.Log("Current Velocity: " + firstPersonMovementComponent.currentVelocity);
                Debug.DrawLine(t.position + new Vector3(0.1f, 0.0f, 0.0f), t.position + interp + new Vector3(0.1f, 0.0f, 0.0f), Color.blue);



            });
        }
    }
}