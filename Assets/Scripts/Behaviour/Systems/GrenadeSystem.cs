﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using System.Timers;

public class GrenadeSystem : ComponentSystem
{
    private List<GameObject> gameObjectsToDestroy = new List<GameObject>();
    protected override void OnUpdate()
    {
        Entities.ForEach<Rigidbody, MeshRenderer, Components.PhysicsProp, Grenade>((Entity e, Rigidbody rigidbody, MeshRenderer m, Components.PhysicsProp physicsProp, Grenade grenade) => {
            if (physicsProp.pickedUp || grenade.active)
            {
                grenade.active = true;
                grenade.elapsedTime += Time.deltaTime;
                m.material.SetColor(Shader.PropertyToID("_Color"), new Color((grenade.elapsedTime / grenade.explosionTime), (grenade.elapsedTime / grenade.explosionTime), (grenade.elapsedTime / grenade.explosionTime)));
                if (grenade.elapsedTime > grenade.explosionTime)
                {
                    var colliders = Physics.OverlapSphere(rigidbody.transform.position, grenade.explosionRadius);
                    foreach(var collider in colliders)
                    {
                        var body = collider.attachedRigidbody;E:\Users\MoustacheSpy\Documents\recoilgame\Assets\Scripts\Behaviour\Systems\FirstPersonMovementSystem.cs
                        var otherGrenade = collider.gameObject.GetComponent<Grenade>();
                        if(body != null)
                        {
                            body.AddExplosionForce(grenade.explosionForce, rigidbody.transform.position, grenade.explosionRadius);
                        }
                        if (otherGrenade != null) { 
                            otherGrenade.active = true;
                            otherGrenade.elapsedTime = otherGrenade.explosionTime / Vector3.Distance(otherGrenade.transform.position, grenade.transform.position);
                        }
                    }
                    
                    PostUpdateCommands.DestroyEntity(e);
                    gameObjectsToDestroy.Add(rigidbody.gameObject);
                }
            }
        });

        for(int i = 0; i < gameObjectsToDestroy.Count; i++)
        {
            MonoBehaviour.Destroy(gameObjectsToDestroy[i]);
            gameObjectsToDestroy.RemoveAt(i);
        }
    }


}
