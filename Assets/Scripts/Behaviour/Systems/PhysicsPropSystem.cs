﻿using Unity.Entities;
using UnityEngine;

namespace Systems
{
    public class PhysicsPropSystem : ComponentSystem, Interactor
    {
        private void physicsMoveTowards(Pickup pickup, Rigidbody body, Components.PhysicsProp prop)
        {
             Vector3 between = pickup.getWorldPickupPosition() - body.transform.position;
             //Debug.Log("BETWEEN: " + between);
             Debug.DrawLine(body.transform.position, body.transform.position + between, Color.red);
             Vector3 targetVelocity = between;
             Vector3 newVelocity = targetVelocity * pickup.pickupPower / prop.pickupDifficulty;

             body.velocity = newVelocity;
            Debug.Log("CAM ROT: " + pickup.transform.rotation);
            if (Mathf.Abs(prop.rotationalForce.magnitude) > 0.05) { 
                body.AddTorque(pickup.transform.localRotation*prop.rotationalForce, ForceMode.VelocityChange);
            }
            else { 
                body.AddTorque(-body.angularVelocity * 0.6f, ForceMode.VelocityChange);
            }
            prop.rotationalForce = new Vector3(0, 0, 0);

        }
        protected override void OnUpdate()
        {
            InputService inputService = new InputService();
            Entities.ForEach<Rigidbody, Components.PhysicsProp>((Entity e, Rigidbody rigidbody, Components.PhysicsProp physicsProp) =>
            {

           /* Debug.Log("PP: " + physicsProp);
            Debug.Log(" | pickup " + physicsProp.pickup);
            Debug.Log(" | pickup Axis: " + physicsProp.pickup.pickupAxis);*/
                bool newKeyState = false;
                if (physicsProp.pickup != null)
                {
                    newKeyState = inputService.getButton(physicsProp.pickup.pickupAxis);
                }
                if (physicsProp.pickedUp == true)
                {
                    if (physicsProp.pickup.pickedUpProp == null)
                    {
                        physicsProp.pickup.pickedUpProp = physicsProp;
                    }
                    else { 
                        if(physicsProp.pickup.pickedUpProp != physicsProp)
                        {
                            Debug.Log("NOT THE SAME PROP");

                            physicsProp.completeInteraction(this);
                        }
                        else { 
                            Debug.Log("LAST KEY STATE : " + physicsProp.pickupKeyHelper + " NEW KEY STATE: " + newKeyState);

                            if (inputService.getButton(InputService.Axes.propRotate))
                            {
                                Vector2 mouse = new Vector2(inputService.getAxis(InputService.Axes.mouseX),
                                    inputService.getAxis(InputService.Axes.mouseY));
                                Debug.Log("MOUSEX :" + mouse);
                                physicsProp.rotationalForce = new Vector3(mouse.y, 0.0f, mouse.x);
                            }

                            if (!physicsProp.pickupKeyHelper && newKeyState) {
                                Debug.Log("USE KEY PRESSED AGAIN!");
                                physicsProp.completeInteraction(this);
                            }
                            else { 
                                Vector3 between = physicsProp.pickup.getWorldPickupPosition() - rigidbody.transform.position;
                                if (between.magnitude > physicsProp.pickup.maxHoldingDistance 
                                || physicsProp.colliding && (physicsProp.collisionObject.relativeVelocity.magnitude * rigidbody.mass) > 100)
                                    physicsProp.completeInteraction(this);
                                else
                                    physicsMoveTowards(physicsProp.pickup, rigidbody, physicsProp);
                                if (inputService.getButton(physicsProp.pickup.throwAxis))
                                {
                                    rigidbody.AddForce(physicsProp.pickup.transform.forward * physicsProp.pickup.throwVelocity);
                                    physicsProp.completeInteraction(this);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if(physicsProp.pickup != null && physicsProp.pickup.pickedUpProp == physicsProp)
                    {
                        physicsProp.pickup.pickedUpProp = null;
                    }
                }
                physicsProp.pickupKeyHelper = newKeyState;

                if (!newKeyState)
                {
                    physicsProp.justDropped = false;
                }
            });
        }

        public void onInteractionCompleted(Interaction interaction, Interaction.Result status)
        {
            
        }

        public void onInteractionStarted(Interaction interaction, Interaction.Result status)
        {
        }

        public void onInteractionHeld(Interaction interaction, Interaction.Result status)
        {
        }
    }
}